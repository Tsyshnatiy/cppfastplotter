// ---------------------------------------------------------------------
// Author: Vladimir Tsyshnatiy 24.08.2014
// ---------------------------------------------------------------------

#ifndef PLOTTER_H
#define PLOTTER_H

#include <stdio.h>
#include <string.h>
#include <tr1/memory>
#include <boost/numpy.hpp>

namespace py = boost::python;
namespace np = boost::numpy;
using namespace std;

template<typename T>
class Plotter
{
protected:
	char* pyPlotScript;
	py::object plotFunc;
	py::object showFunc;
	bool takePythonOwnership;
	static tr1::shared_ptr<Plotter<T> > instance;
	
	Plotter(const Plotter&){} //forbid copying
	
	//singleton
	Plotter(const char* pyPlotScript, 
			bool takePythonOwnership,
			const char* plotFncName,
			const char* showFncName)
	{
		this->takePythonOwnership = takePythonOwnership;
		if (takePythonOwnership)
		{
			Py_Initialize();
			np::initialize();
		}
		setPyPlotScript(pyPlotScript, plotFncName, showFncName);
	}

public:
	static tr1::shared_ptr<Plotter<T> >& getInstance(const char* pyPlotScript, 
								bool takePythonOwnership = true,
								const char* plotFncName = "plot",
								const char* showFncName = "show")
	{
		if (instance.get() == NULL)
		{
			instance = tr1::shared_ptr<Plotter<T> >(new Plotter<T>(pyPlotScript, 
										takePythonOwnership,
										plotFncName, showFncName));
		}
		
		return instance;
	}
	
	void setPyPlotScript(const char* pyScr, 
							const char* plotFncName = "plot",
							const char* showFncName = "show") 
	{ 
		pyPlotScript = new char[strlen(pyScr)];
		strcpy(pyPlotScript, pyScr);
		py::object main = py::import("__main__");
		py::object scope = main.attr("__dict__");
				
		try
		{
			py::exec_file(pyPlotScript, scope, scope);
		}
		catch (py::error_already_set const& )
		{
			PyErr_Print();
		}
		
		plotFunc = scope[plotFncName];
		showFunc = scope[showFncName];
	}
	
	virtual void plot(const T* const x, const T* const y, const size_t arrSize)
	{
		py::tuple shape = py::make_tuple(arrSize);
		np::dtype dtype = np::dtype::get_builtin<T>();
		np::ndarray xPy = np::empty(shape, dtype);
		np::ndarray yPy = np::empty(shape, dtype);
		
		//copy data. ndarray::from_data does not work properly!
		//also data copying is good for thread safety
		memcpy(reinterpret_cast<T*>(xPy.get_data()), x, arrSize * sizeof(T));
		memcpy(reinterpret_cast<T*>(yPy.get_data()), y, arrSize * sizeof(T));
		
		plotFunc(xPy, yPy);
	}
	
	void show()
	{
		showFunc();
	}
	
	virtual ~Plotter()
	{
		delete [] pyPlotScript;
		if (takePythonOwnership)
		{
			Py_Finalize();
		}
	}
};

template<typename T>
tr1::shared_ptr<Plotter<T> > Plotter<T>::instance;

#endif //PLOTTER_H
