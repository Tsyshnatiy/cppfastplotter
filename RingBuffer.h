#ifndef RINGBUFFER_H
#define RINGBUFFER_H

#include <iostream>
using namespace std;

// ---------------------------------------------------------------------
// Author: Vladimir Tsyshnatiy 24.08.2014
// ---------------------------------------------------------------------

template<class T>
class RingBuffer
{
private:
    T* buffer;
    int size;
    int elemsSet;
public:
    RingBuffer(const int size, const T defVal);
    RingBuffer(const RingBuffer& other); //copy constructor
    RingBuffer(const T* initCollection, const int colSize, const int bufSize);
    explicit RingBuffer(const int size);

    virtual ~RingBuffer();
public:
    int getSize() const { return size; }
    T* getBuffer() const { return buffer; }
    int getElemsSet() const { return elemsSet; }
    void push(const T item); //copies item to container
    T* sort() const; //sorts in ascending order
    void print(ostream& strm);
    void fill(T value);
};

#endif // RINGBUFFER_H
