// ---------------------------------------------------------------------
// Author: Vladimir Tsyshnatiy 24.08.2014
// ---------------------------------------------------------------------

#include "Plotter.h"
#include <math.h>
#include <tr1/memory>
#include "RingBuffer.h"
#include "RingBuffer.cpp"

#define FUNC1(x) sin(x)
#define NODES 1000
#define WINDOW1 40
#define WINDOW2 20

using namespace std;

double getAveraged(const RingBuffer<double>* const rb)
{
	double* bf = rb->getBuffer();
	double sum = 0;
	for (int i = rb->getSize() ; i >= 0 ; --i)
	{
		sum += bf[i];
	}
	return sum / rb->getSize();
}

int main()
{
	srand (time(NULL));
	RingBuffer<double>* rb = new RingBuffer<double>(WINDOW1);
	RingBuffer<double>* rbf = new RingBuffer<double>(WINDOW2);
	tr1::shared_ptr<Plotter<double> > p = Plotter<double>::getInstance("simple_plotter.py");
	double x[NODES];
	double y[NODES];
	double yf[NODES];
	double yff[NODES];
	
	double left = -1 * M_PI;
	double right = 2 * M_PI;
	double step = (right - left) / (NODES - 1);

	for (int i = 0 ; i < NODES ; ++i)
	{
		int isNegative = rand() % 2;
		float randDiv = (rand() % 7) + 1;
		float random = (rand() % 10) / randDiv;
		x[i] = i * step + left;
		y[i] = isNegative == 1 ? FUNC1(x[i]) - random : FUNC1(x[i]) + random;
		rb->push(y[i]);
		yf[i] = getAveraged(rb);
		rbf->push(yf[i]);
		yff[i] = getAveraged(rbf);
	}

	p->plot(x, y, NODES);
	p->plot(x, yf, NODES);
	p->plot(x, yff, NODES);
	p->show();
	
	delete rbf;
	delete rb;
}
