// ---------------------------------------------------------------------
// Author: Vladimir Tsyshnatiy 24.08.2014
// ---------------------------------------------------------------------

#include "Plotter.h"
#include <math.h>
#include <tr1/memory>

#define FUNC1(x) sin(x)
#define FUNC2(x) cos(x)
#define NODES 500

using namespace std;

int main()
{
	tr1::shared_ptr<Plotter<double> > p = Plotter<double>::getInstance("simple_plotter.py");
	double x[NODES];
	double y[NODES];
	double left = -1 * M_PI;
	double right = M_PI;
	double step = (right - left) / (NODES - 1);

	for (int i = 0 ; i < NODES ; ++i)
	{
		x[i] = i * step + left;
		y[i] = FUNC1(x[i]);
	}

	p->plot(x, y, NODES);
	
	for (int i = 0 ; i < NODES ; ++i)
	{
		x[i] = i * step + left;
		y[i] = FUNC2(x[i]);
	}
	
	p->plot(x, y, NODES);
	
	p->show();
}
