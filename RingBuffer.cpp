// ---------------------------------------------------------------------
// Author: Vladimir Tsyshnatiy 24.08.2014
// ---------------------------------------------------------------------

#include "RingBuffer.h"
#include <algorithm>

template<typename T>
RingBuffer<T>::RingBuffer(const int size, const T defVal)
{
    this->size = size;
    buffer = new T[size];
    fill(buffer, buffer + size, defVal);
}

template<typename T>
RingBuffer<T>::RingBuffer(const int size)
{
    this->size = size;
    buffer = new T[size];
}

template<typename T>
RingBuffer<T>::RingBuffer(const T* initCollection, const int colSize, const int bufSize)
{
    this->size = bufSize;
    buffer = new T[bufSize];
    for (int i = 0 ; i < colSize ; ++i)
    {
        push(initCollection[i]);
    }
}

template<typename T>
RingBuffer<T>::RingBuffer(const RingBuffer& other)
{
    //delete [] buffer; Memory leak?
    buffer = new T[other.size];
    this->size = other.size;
    copy(other.buffer, other.buffer + other.size, this->buffer);
}

template<typename T>
RingBuffer<T>::~RingBuffer()
{
    delete [] buffer;
    buffer = NULL;
}

template<typename T>
void RingBuffer<T>::push(const T item)
{
    for (int i = 0 ; i < size - 1 ; ++i)
    {
        swap(buffer[i], buffer[i + 1]);
    }
    buffer[size - 1] = item;
    elemsSet++;
    if (elemsSet > size)
    {
		elemsSet = size;
	}
}

template<typename T>
void RingBuffer<T>::fill(T value)
{
    std::fill(buffer, buffer + size, value);
}

template<typename T>
T* RingBuffer<T>::sort() const
{
    T* bufTmp = new T[size];
    copy(buffer, buffer + size, bufTmp);
    std::sort(bufTmp, bufTmp + size);
    return bufTmp;
}

template<typename T>
void RingBuffer<T>::print(ostream& strm)
{
    strm << "Ring buffer" << endl;
    for (int i = 0 ; i < size ; ++i)
    {
        strm << buffer[i] << endl;
    }
}
