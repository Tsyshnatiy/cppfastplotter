#CppFastPlotter
This library is created to allow fast plotting from C++ programs.
Plotting is based on python2.7 and matplotlib.
The main idea is:
You have cpp program that computes a lot of data.
It's good to plot this data in a separate window.
You get this lib, install it and modify python plotting script for your needs.
Lib contains demo_plotter to show usage.
Author will be glad if you save his header at the top of lib's source files.

#Install
You will need boost, python2.7 (maybe 3 will work, need to check this), matplotlib python package.
Also you will need numpy C++ library.
You can get it from https://github.com/ndarray/Boost.NumPy.git
Build it via cmake with prefix /usr/include.
To use lib just include Plotter.h into your source.
To build demo use buildme.sh. But it's good to take fast look into that script.

#Notes 
Lib is totally free.
Lib is tested under linux only, but it does not use any linux
specific things, so theritically it can be ported to windows (for example) easily.
You can build several plots using this lib.
